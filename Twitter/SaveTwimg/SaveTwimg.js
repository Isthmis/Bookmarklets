void (() => {
    `use strict`;
    const flag = {
        pbs       : location.hostname === `pbs.twimg.com`,
        pc        : location.hostname === `twitter.com`,
        mobile    : location.hostname === `mobile.twitter.com`,
        tweetdeck : location.hostname === `tweetdeck.twitter.com`,
    };
    const getOrigURL = target =>
        // HTML Element
        target.tagName ?
            // <img>
            /img/i.test(target.tagName)      ? getOrigURL(target.src)
            // <video>
            : /video/i.test(target.tagName)  ? getOrigURL(target.poster)
            // <iframe>
            : /iframe/i.test(target.tagName) ? getOrigURL(target.contentWindow.document.querySelector(`img[src*="/card_img/"], img[src*="/ad_img/"]`))
            // TweetDeck .media-image
            : flag.tweetdeck && [`media-image`, `prf-header`].some(className => target.classList.contains(className)) ? getOrigURL(target.style.backgroundImage.replace(/url\(|\)/g, ``))
            // Exception
            : null
        // URL
        : typeof target === `string` ?
            // _400x400.jpg (Profile Image)
            /\/profile_images\//.test(target)    ? target.replace(/_((\d+)?x\d+|reasonably_small|normal|bigger)(?=\.)/, ``)
            // /1500x500, /web (Profile Banner)
            : /\/profile_banners\//.test(target) ? /\/\d+$/.test(target) ? target + `/1500x500` : target.replace(/[^\/]*$/, `1500x500`)
            // Twitter Card, Ad
            : /\/(card|ad)_img\//.test(target)   ? target.replace(/name=\w+/, `name=orig`)
            // ?format=jpg&name=orig
            : /\?/.test(target)                  ? target.replace(/\?.*/, ``) + (/format=\w+/.test(target) ? `.` + target.match(/format=(\w+)/)[1] : ``) + `?name=orig`
            // .jpg, .jpg:orig
            : /\..+$/.test(target)               ? target.replace(/:\w+$/, ``) + `?name=orig`
            // Exception
            : null
        // Exception
        : null
    ;

    // pbs.twimg.com
    if ( flag.pbs ) {
        const currentURL = location.href;
        const    origURL = getOrigURL(currentURL);
        if (currentURL !== origURL) location.assign(origURL);
    }
    // twitter.com, mobile.twitter.com, tweetdeck.twitter.com
    else if ( flag.pc || flag.mobile || flag.tweetdeck ) {
        const origURLs = [...
            // twitter.com, mobile.twitter.com
            flag.pc || flag.mobile ?
                // Gallery - Photo
                /\/photo\//.test(location.pathname) ? [document.querySelectorAll(`img[src*="/media/"]:not([data-test-id="primaryColumn"] img)`)[location.pathname.slice(-1) - 1]]
                // Gallery - Profile Image
                : /\/photo$/.test(location.pathname) ? [document.querySelector(`img[src*="/profile_images/"]`)]
                // Tweet Detail Page, Tweet Summary Page (?s=...) - Photo, GIF (tweet_video_thumb), Video (ext_tw_video_thumb), Card, Ad
                : /\/status\//.test(location.pathname) ?
                    [...
                        document.querySelector(`article a[href="${location.pathname}" i]`)
                        .closest(`article`)
                        .querySelectorAll(`a[href*="/photo/"] img[src*="/media/"], a[target="_blank"] img[src*="/media/"], img[src*="video_thumb/"], img[src*="/card_img/"], img[src*="/ad_img/"]`)
                    ].sort((a, b) => a.closest(`a`).href.slice(-1) - b.closest(`a`).href.slice(-1))
                // Profile Page - Profile Banner
                : [document.querySelector(`img[src*="/profile_banners/"]`)]
            // tweetdeck.twitter.com
            :
                // Profile Page - Profile Image, Profile Banner
                document.querySelector(`.prf-header`) ? [...document.querySelectorAll(`.prf-header, .prf-header .avatar`)].reverse()
                // Gallery
                : document.querySelector(`#open-modal .media-img`) ? [document.querySelector(`#open-modal .media-img`)]
                // Tweet Detail Page
                : !document.querySelector(`#open-modal`).childElementCount ? document.querySelectorAll(`.tweet-detail-media .media-img, .tweet-detail-media .media-image`)
                // Tweet Detail Page (Modal)
                : document.querySelectorAll(`#open-modal .tweet-detail-media .media-img, #open-modal .tweet-detail-media .media-image`)
        ].map(content => getOrigURL(content));

        // OPEN
        // - Multiple Images
        if ( 1 < origURLs.length ) {
            const orig =
                `<!DOCTYPE html>
                <title>*${origURLs.length} Twimgs*</title>
                <style>
                    html, body, img {
                        max-width: 100%;
                    }
                    html {
                        -webkit-user-select: none;
                        user-select: none;
                    }
                    img {
                        display: block;
                        margin: 0 auto;
                    }
                    hr {
                        border: none;
                        border-top: 12px dashed;
                        margin: 40px 0;
                    }
                    @media (prefers-color-scheme: dark) {
                        html {
                            background-color: black;
                        }
                        hr {
                            border-color: white;
                        }
                    }
                </style>
                <img src="${origURLs.join(`" /><hr /><img src="`)}" />`
            ;
            orig && open().document.write(orig);
        }
        // - Single Image
        else {
            const orig = origURLs[0];
            orig && open(orig);
        }
    }
})();
