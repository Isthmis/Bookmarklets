# 🐣 Save Twimg

**★ Bookmarklet**

Save original Twitter images bookmarklet

- ### Original Image

    - `TWIMG?format=jpg&name=large`
    - `TWIMG.jpg:large`
    - `TWIMG.jpg`

        → `TWIMG.jpg?name=orig`

- ### Original Profile Image

    - `TWIMG_400x400.jpg`

        → `TWIMG.jpg`

- ### Open Original Images from Tweet Page

    - Single Image → Open `pbs.twimg.com/media/TWIMG.jpg?name=orig`
    - Multiple Images → Open the Images List (`data:text/html,…`)  
    // If you are using Firefox Focus (Content Blocker for iOS), you need to disable it.

- ### Open Original Image from Image Gallery

    - Image Gallery
    - Profile Image Gallery

- ### Video Thumbnail

    - GIF Thumbnail, Video Thumbnail (`pbs.twimg.com`)
        - GIF → Open `pbs.twimg.com/tweet_video_thumb/TWIMG.jpg:orig`
        - Video → Open `pbs.twimg.com/ext_tw_video_thumb/…/pu/img/TWIMG.jpg:orig`

## ◆ Copy to Bookmarks

[SaveTwimg.bookmarklet](SaveTwimg.bookmarklet)

## ◆ License

This project is licensed under the terms of the CC0-1.0 Universal license.

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg "CC0")](https://creativecommons.org/publicdomain/zero/1.0/)
