# 🐣 Save Twimg BM

**★ Bookmarklet**

Save original Twitter images bookmarklet

## ◆ Copy to Bookmarks

[SaveTwimg BM.bookmarklet](SaveTwimg%20BM.bookmarklet)

## ◆ License

This project is licensed under the terms of the CC0-1.0 Universal license.

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg "CC0")](https://creativecommons.org/publicdomain/zero/1.0/)
