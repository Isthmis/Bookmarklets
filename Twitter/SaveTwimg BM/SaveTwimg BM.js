void (() => {
    `use strict`;
    const flag = {
        pbs    : location.hostname === `pbs.twimg.com`,
        pc     : location.hostname === `twitter.com`,
        mobile : location.hostname === `mobile.twitter.com`,
        BM     : Boolean(window.saveTwimgBM),
    };
    const getOrigURL = target =>
        // HTML Element
        target.tagName ?
            // <iframe>
            /iframe/i.test(target.tagName)  ? getOrigURL(target.contentWindow.document.querySelector(`img[src*="/card_img/"], img[src*="/ad_img/"]`))
            // <video>
            : /video/i.test(target.tagName) ? getOrigURL(target.poster)
            // <img>
            : /img/i.test(target.tagName)   ? getOrigURL(target.src)
            // Exception
            : null
        // URL
        : typeof target === `string` ?
            // _400x400.jpg (Profile Image)
            /\/profile_images\//.test(target)  ? target.replace(/_((\d+)?x\d+|reasonably_small|normal|bigger)(?=\.)/, ``)
            // /1500x500 (Profile Banner)
            : /\/\d+x\d+$/.test(target)        ? target.replace(/\/\d+x\d+$/, `/1500x500`)
            // Twitter Card, Ad
            : /\/(card|ad)_img\//.test(target) ? target.replace(/name=\w+/, `name=orig`)
            // ?format=jpg&name=orig
            : /\?/.test(target)                ? target.replace(/\?.*/, ``) + (/format=\w+/.test(target) ? `.` + target.match(/format=(\w+)/)[1] : ``) + `?name=orig`
            // .jpg, .jpg:orig
            : /\..+$/.test(target)             ? target.replace(/:\w+$/, ``) + `?name=orig`
            // Exception
            : null
        // Exception
        : null
    ;
    const openOrig = imgURLs => {
        if (!imgURLs) return;
        if (!imgURLs[Symbol.iterator]) imgURLs = [imgURLs];
        const origURLs = [...imgURLs].map(URL => getOrigURL(URL));

        // OPEN
        // OPEN - Multiple Images
        if ( 1 < origURLs.length ) {
            const orig =
                `<!DOCTYPE html>
                <title>*${origURLs.length} Twimgs*</title>
                <style>
                    html, body, img {
                        max-width: 100%;
                    }
                    html {
                        -webkit-user-select: none;
                        user-select: none;
                    }
                    img {
                        display: block;
                        margin: 0 auto;
                    }
                    hr {
                        border: none;
                        border-top: 12px dashed;
                        margin: 40px 0;
                    }
                    @media (prefers-color-scheme: dark) {
                        html {
                            background-color: black;
                        }
                        hr {
                            border-color: white;
                        }
                    }
                </style>
                <img src="${origURLs.join(`" /><hr /><img src="`)}" />`
            ;
            orig && open().document.write(orig);
        }
        // OPEN - Single Image
        else {
            const orig = origURLs[0];
            orig && open(orig);
        }
    };



    if ( flag.pbs ) {
        const currentURL = location.href;
        const    origURL = getOrigURL(currentURL);
        if (currentURL !== origURL) location.assign(origURL);
    } else if ( flag.mobile ) {
        if (flag.BM) return void window.saveTwimgBM.save();

        // Gallery
        // Gallery - Photo
        if (/\/photo\//.test(location.pathname)) return void openOrig(document.querySelectorAll(`img[src*="/media/"]:not([data-test-id="primaryColumn"] img)`)[location.pathname.slice(-1) - 1]);
        // Gallery - Profile Image
        if (/\/photo$/.test(location.pathname))  return void openOrig(document.querySelector(`img[src*="/profile_images/"]`));



        (new class {
            display = document.querySelector(`main`);
            submitButton        = document.querySelector(`[href="/compose/tweet"]`);
            submitButton_backup = this.submitButton && this.submitButton.outerHTML;
            backButton        = document.querySelector(`header [role="presentation"]`) || document.querySelector(`header svg`)?.closest(`[role="button"]`);
            backButton_backup = this.backButton && this.backButton.outerHTML;
            imgURLs = [];
            eventListeners = {};
            transitionDuration = 500;
            transitionendAfterWaitDuration = 200;
            sessionID = setInterval((() => {
                let prevURL = location.href;
                return () => {
                    if (prevURL !== location.href) this.exit();
                    else prevURL = location.href;
                }
            })(), 100);
            createOverlay(type) {
                if (!document.head.querySelector(`.saveTwimgBM_overlay`)) document.head.insertAdjacentHTML(`beforeend`, `
                    <style class="saveTwimgBM_overlay">
                        [class^="saveTwimgBM_overlay_"] {
                            z-index: 1000;
                            position: fixed;
                            top: 0;
                            right: 0;
                            bottom: 0;
                            left: 0;
                            width: 100%;
                            height: 100%;
                            display: flex;
                            align-items: center;
                            justify-content: center;
                            text-align: center;
                            color: white;
                            font-size: 36px;
                            font-family: sans-serif;
                            -webkit-backdrop-filter: blur(4px);
                                    backdrop-filter: blur(4px);
                            background: rgba(0, 0, 0, 0.8);
                            transition: opacity ${this.transitionDuration}ms ease-out;
                            opacity: 0;
                        }
                        [class^="saveTwimgBM_message_"] {
                            opacity: 0;
                            transition: all ${this.transitionDuration}ms ease-out;
                            transform: translateY(-12px);
                        }
                        [class^="saveTwimgBM_overlay_"].visible {
                            opacity: 1;
                        }
                        [class^="saveTwimgBM_message_"].visible {
                            opacity: 1;
                            transform: translateY(0);
                        }
                    </style>
                `);
                if (!document.body.querySelector(`.saveTwimgBM_overlay_` + type)) document.body.insertAdjacentHTML(`beforeend`,
                    `<div class="saveTwimgBM_overlay_${type}">` + (
                        type === `hello` ?
                            `<div class="saveTwimgBM_message_hello">
                                <span style="font-size: 200%;">🔥🐣🔥</span><br />
                                <span style="font-style: italic;">BURNING MODE</span>
                            </div>`
                        :
                            `<div class="saveTwimgBM_message_bye">
                                <span style="font-size: 200%;">🚒💨</span><br />
                                <span style="font-style: italic;">CANCELED</span>
                            </div>`
                    ) + `</div>`
                );
                return document.body.querySelector(`.saveTwimgBM_overlay_` + type);
            }
            save() {
                if (!this.imgURLs.length) return void this.cancel();

                this.exit();
                openOrig(this.imgURLs);
            }
            exit() {
                flag.BM = false;
                clearInterval(this.sessionID);
                document.querySelectorAll(`[class^="saveTwimgBM_overlay"], style[data-saveTwimgBM]`).forEach(element => element.remove());
                this.eventListeners.selectImages && document.removeEventListener(...this.eventListeners.selectImages);
                if ( this.submitButton ) {
                    document.removeEventListener(...this.eventListeners.save);
                    this.submitButton.outerHTML = this.submitButton_backup;
                }
                if ( this.backButton ) {
                    document.removeEventListener(...this.eventListeners.cancel);
                    this.backButton.outerHTML = this.backButton_backup;
                }
                this.eventListeners.preventPageTransition && document.removeEventListener(...this.eventListeners.preventPageTransition);
                delete window.saveTwimgBM;
            }
            cancel() {
                const overlay = this.createOverlay(`bye`);
                setTimeout(() => {
                    overlay.classList.add(`visible`);
                    overlay.querySelectorAll(`div`).forEach(element => element.classList.toggle(`visible`));
                    overlay.addEventListener(`transitionend`, () => setTimeout(() => {
                        overlay.classList.remove(`visible`);
                        overlay.addEventListener(`transitionend`, () => {
                            this.exit();
                        }, {once: true});
                    }, this.transitionendAfterWaitDuration), {once: true});
                });
            }
            start() {
                if (!this.display) return void this.exit();
                window.saveTwimgBM = this;
                document.querySelectorAll(`
                    a[href="/${location.pathname.slice(1).replace(/\/.+/, ``)}/photo"] > div[role="presentation"] > :not(:first-child),
                    [data-testid="tweet"] a[href^="/"] > :not(:first-child)
                `).forEach(obstacle => obstacle.remove());



                /*
                    OVERLAY
                */
                const overlay = this.createOverlay(`hello`);
                setTimeout(() => {
                    overlay.classList.add(`visible`);
                    overlay.querySelector(`.saveTwimgBM_message_hello`).classList.add(`visible`);
                });
                overlay.addEventListener(`transitionend`, () => setTimeout(() => {
                    overlay.classList.remove(`visible`);
                    overlay.addEventListener(`transitionend`, () => overlay.remove(), {once: true});
                }, this.transitionendAfterWaitDuration), {once: true});



                /*
                    STYLES
                */
                if ( this.submitButton ) {
                    this.submitButton.style.backgroundColor = `#f2681d`;
                    this.submitButton.firstElementChild.textContent = `🐥`;
                    this.submitButton.firstElementChild.style.fontSize = `24px`;
                }
                if ( this.backButton ) {
                    this.backButton.textContent = `🚒`;
                    this.backButton.style.display = `flex`;
                    this.backButton.style.alignItems = `center`;
                    this.backButton.style.justifyContent = `center`;
                    this.backButton.style.fontSize = `200%`;
                }



                /*
                    EVENTS
                */
                // Select Images
                document.addEventListener(...(() =>
                    this.eventListeners.selectImages = [`click`, event => {
                        event.preventDefault();
                        const img =
                            // <img>
                            /img/i.test(event.target.tagName) ? event.target
                            // Profile Photo (Header)
                            : event.target.closest(`a[href$="/photo"]`) ? event.target.closest(`a[href$="/photo"]`).querySelector(`img[src*="/profile_images/"]`)
                            // Profile Photo (Tweet)
                            : /^\/[a-zA-Z\d_]+$/.test(event.closest(`a`)?.href) ? event.closest(`a`).querySelector(`img[src*="/profile_images/"]`)
                            // Exception
                            : null
                        ;

                        if (!img) return;

                        event.stopPropagation();
                        if (this.imgURLs.includes(img.src)) this.imgURLs.splice(this.imgURLs.indexOf(img.src), 1);
                        else this.imgURLs.push(img.src);

                        // Refresh Lock-On Effect
                        const styleElement = document.head.querySelector(`style[data-saveTwimgBM]`);
                        const style = `[style*="${this.imgURLs.join(`"], [style*="`)}"] {opacity:0.5}`;
                        if (!styleElement) document.head.insertAdjacentHTML(`beforeend`, `<style data-saveTwimgBM>${style}</style>`);
                        else styleElement.textContent = style;
                    }, true]
                )());
                // Cancel
                this.backButton && document.addEventListener(...(() =>
                    this.eventListeners.cancel = [`click`, event => {
                        if (!this.backButton.contains(event.target)) return;
                        event.preventDefault();
                        event.stopPropagation();

                        this.cancel();
                    }, true]
                )());
                // Save
                this.submitButton && document.addEventListener(...(() =>
                    this.eventListeners.save = [`click`, event => {
                        if (!this.submitButton.contains(event.target)) return;
                        event.preventDefault();
                        event.stopPropagation();

                        this.save();
                    }, true]
                )());
                // Prevent Page Transition
                document.addEventListener(...(() =>
                    this.eventListeners.preventPageTransition = [`click`, event => {
                        event.preventDefault();
                        event.stopPropagation();
                    }, true]
                )());
            }
        }).start();
    }
})();
