void (() => {
    `use strict`;

    if ( !/\/status\//.test(location.pathname) ) return;

    const simpleDialog = (() => {
        document.body.insertAdjacentHTML(`beforeend`, `
            <div id="simpleDialog" style="
                    z-index: 1000;
                    position: fixed;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    transition: opacity 0.5s;
                    box-sizing: border-box;
            ">
                <div id="simpleDialog_overlay" style="
                    z-index: 1000;
                    position: fixed;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    text-align: center;
                    font-size: 36px;
                    font-family: sans-serif;
                    color: white;
                    background: rgba(0, 0, 0, 0.8);
                    -webkit-backdrop-filter: blur(4px);
                    backdrop-filter: blur(4px);
                    transition: opacity 0.5s;
                    opacity: 0;
                    cursor: pointer;
                    box-sizing: border-box;
                "></div>
                <div id="simpleDialog_body" style="
                    z-index: 1000;
                    position: fixed;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    margin: auto;
                    width: 75%;
                    height: 75%;
                    max-width: 500px;
                    max-height: 400px;
                    padding: 1em;
                    font-size: 16px;
                    font-family: sans-serif;
                    color: white;
                    background: dimgray;
                    transition: opacity 0.5s;
                    opacity: 0;
                    box-sizing: border-box;
                ">
                <h1 contenteditable>タイトル</h1>
                <hr />
                <div contenteditable style="
                    padding: 1em;
                    font-size: 16px;
                    font-family: sans-serif;
                    color: white;
                    background: dimgray;
                    box-sizing: border-box;
                ">テキスト</div>
                </div> 
            </div>
        `);

        document.querySelectorAll(`#simpleDialog_overlay, #simpleDialog_body`).forEach(element => element.style.opacity = `1`);

        document.querySelector(`#simpleDialog_overlay`).addEventListener(`click`, () => {
            const dialog = document.querySelector(`#simpleDialog`);

            dialog.addEventListener(`transitionend`, () => {
                dialog.remove();
            }, {once: true});

            dialog.style.opacity = `0`;
        }, {once: true});
    })();

    const contains = (target, text) => {
        let _target;
        if ( target instanceof Element ) {
            _target = [target];
        } else if ( target instanceof NodeList || target instanceof HTMLCollection ) {
            _target = [...target];
        } else {
            _target = [...document.querySelectorAll(target)];
        }

        return _target.filter(element =>
            RegExp(text).test(element.textContent)
        )
    };

    const tweet = document.querySelector(`a[href="https://help.twitter.com/using-twitter/how-to-tweet#source-labels"]`).closest(`article`);
    const tweetUserName = tweet.querySelector(`span`).textContent;
    const tweetUserID = tweet.querySelector(`a`).getAttribute(`href`).slice(1);
    const tweetText = (() => {
        const tweetText = tweet.querySelector(`[lang]`);

        tweetText.querySelectorAll(`img[alt]`).forEach(img => {
            const span = document.createElement(`span`);
            span.textContent = img.alt;
            img.parentElement.replaceChild(span, img);
        });

        return tweetText.textContent;
    })();
    const tweetDateTime = (() => {
        const element = contains(tweet.querySelectorAll(`span`), ` · `).slice(-1)[0];
        const dateString = element.textContent.split(` · `).reverse().join(` `);

        return new Date(dateString);
    })();
    // YYYY/MM/DD HH:mm
    const tweetDateTimeString = (() => {
        const YYYY = tweetDateTime.getFullYear();
        const MM = `0${tweetDateTime.getMonth() + 1}`.slice(-2);
        const DD = `0${tweetDateTime.getDate()}`.slice(-2);
        const HH = `0${tweetDateTime.getHours()}`.slice(-2);
        const mm = `0${tweetDateTime.getMinutes()}`.slice(-2);

        return `${YYYY}/${MM}/${DD} ${HH}:${mm}`;
    })();

    const metadata = `${tweetUserName} (@${tweetUserID}) ${tweetDateTimeString}`;
    const url = document.head.querySelector(`link[rel="canonical"]`).href.replace(/^https:\/\/mobile\./, `https://`);

    prompt(`Copy Tweet Metadata`,
        metadata + `\n` +
        url + `\n` +
        tweetText
    );
})();
