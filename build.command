#!/bin/bash



cd "$(dirname "$0")"

if [ ! -d node_modules ]; then
    npm install && npm start
else
    npm start
fi
