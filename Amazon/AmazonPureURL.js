void (() => {
    `use strict`;
    const ASIN = (location.pathname + `/`).match(/\/[0-9A-Z]{10}\//)[0].slice(1, -1);
    const pureURL = `https://www.amazon.co.jp/dp/` + ASIN;
    if (location.href !== pureURL) location.assign(pureURL);
})();
