void (() => {
    `use strict`;

    const macbenchURL = `https://browser.geekbench.com/mac-benchmarks`;
    const pureURL = location.protocol + `//` + location.host + location.pathname;
    if ( pureURL !== macbenchURL ) return location.assign(macbenchURL);

    const searchQuery = prompt(`Filter:`).trim().toLowerCase();
    if ( !searchQuery ) return;

    const macs = document.querySelectorAll(`.mac-benchmark td.name`);
    for ( const mac of macs ) {
        const content = mac.textContent.toLowerCase();
        if ( !content.includes(searchQuery) ) {
            mac.closest(`tr`).remove();
        }
    }
})();
