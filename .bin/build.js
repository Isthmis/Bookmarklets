const babel      = require(`@babel/core`);
const fs         = require(`fs`);
const glob       = require(`glob`);
const micromatch = require(`micromatch`);
const htmlmin    = require(`html-minifier`).minify;
const path       = require(`path`);
const readline   = require(`readline`);
const shell      = require(`shelljs`);
const watch      = require(`watch`);

const packagejson = require(process.env.PWD + path.sep + `package.json`);
const config = packagejson.watch || {};
if (!config.target)         config.target         = [`**`];
if (!config.ignore)         config.ignore         = [`**/node_modules/**`, `**/package.json`, `**/package-lock.json`, `README.md`];
if (!config.ignoreDotFiles) config.ignoreDotFiles = false;
const flag = {
    buildAll: false,
    clock: false,
};
const indent = (n = 1) => ` `.repeat(4).repeat(n);

console.log(`ignore: ` + config.ignore.join(`, `));
console.log(``);
console.log(indent() + `- Build (Auto)......UPDATE FILES`);
console.log(indent() + `- Build All.........PRESS ENTER`);



/*
    CLOCK
*/
let loggedTime;
const clock = (() => {
    let time;
    const passed = () => {
        if (!loggedTime) return ``;
        const diffTime = (time - loggedTime) / 1000;
        const diffDay  = Math.floor(diffTime / 86400);
        const diffHour = Math.floor(diffTime / 3600 % 24);
        const diffMin  = Math.floor(diffTime / 60 % 60);
        const diffSec  = Math.floor(diffTime % 60);
        const diffString =
            (diffDay  ? diffDay  + (1 < diffDay  ? `days `  : `day ` ) : ``) +
            (diffHour ? diffHour + (1 < diffHour ? `hours ` : `hour `) : ``) +
            (diffMin  ? diffMin  + (1 < diffMin  ? `mins `  : `min ` ) : ``) +
            (diffSec  ? diffSec  + (1 < diffSec  ? `secs `  : `sec ` ) : ``)
        ;
        return diffString ? ` (` + diffString + `passed.)` : ` (NOW COMPLETED!)`;
    };
    return () => {
        if ( flag.clock ) {
            time = new Date;
            process.stdout.clearLine();
            process.stdout.cursorTo(0);
            process.stdout.write(indent() + time.toLocaleString() + passed());
        }
        setTimeout(clock, 1000);
    };
})();
clock();



/*
    BUILD
*/
const build = (event, file = ``) => {
    if (flag.buildAll) event = `build all`;
    flag.clock = false;
    loggedTime = new Date;
    switch ( true ) {
        case !buildCounter && !flag.buildAll:
        case buildCounter && flag.buildAll:
            console.log(``);
            break;
        case buildCounter && !flag.buildAll:
            console.log(`\n`);
            break;
    }
    console.log(`[!]`, `#` + ++buildCounter, event, file);
    console.log(indent() + loggedTime.toLocaleString());
    // All
    if ( flag.buildAll ) {
        const files = glob.sync(`**/*`, {
            dot: !config.ignoreDotFiles,
            ignore: config.ignore,
            nodir: true,
        });
        shell.rm(...files.filter(file => path.extname(file) === `.bookmarklet`));
        const codes = files.filter(file => [`.js`, `.html`].includes(path.extname(file)));
        const bookmarklets = codes.map(code => code.replace(/\.[a-z\d]+$/, `.bookmarklet`));
        console.log(``);
        console.log(indent() + `Build:`);
        for ( let i = 0; i < codes.length; i++ ) {
            switch ( path.extname(codes[i]) ) {
                case `.js`:
                    const transformed = (() => {
                        try {
                            return babel.transformFileSync(codes[i]).code;
                        } catch ( error ) {
                            console.error(error);
                            return ``;
                        }
                    })();
                    fs.writeFileSync(bookmarklets[i], `javascript:` + encodeURIComponent(transformed));
                    break;
                case `.html`:
                    const minified = (() => {
                        try {
                            return htmlmin(fs.readFileSync(codes[i], `utf-8`), {
                                html5: true,
                                removeStyleLinkTypeAttributes: true,
                                removeScriptTypeAttributes: true,
                                minifyCSS: true,
                                minifyJS: (text, inline) => {
                                    if (inline) return text;
                                    return babel.transform(text, packagejson.babel).code;
                                },
                                removeComments: true,
                                collapseWhitespace: true,
                                removeTagWhitespace: true,
                            });
                        } catch ( error ) {
                            console.error(error);
                            return ``;
                        }
                    })();
                    fs.writeFileSync(bookmarklets[i], `data:text/html,` + encodeURIComponent(minified));
                    break;
            }
            console.log(indent(2) + `-`, codes[i]);
        }
    }
    // One
    else {
        const js = file.replace(/\.[a-z\d]+$/, `.js`);
        const html = file.replace(/\.[a-z\d]+$/, `.html`);
        const bookmarklet = file.replace(/\.[a-z\d]+$/, `.bookmarklet`);
        if ( fs.existsSync(js) && fs.existsSync(html) ) {
            console.error(`\n${indent(1)}Duplicate file names!!⚡️😱😱⚡️\n${indent(2)}🔥 ${js}\n${indent(2)}🔥 ${html}\n`);
            process.exit(1);
        }
        // .js
        else if ( fs.existsSync(js) ) {
            const transformed = (() => {
                try {
                    return babel.transformFileSync(js).code;
                } catch ( error ) {
                    console.error(error);
                    return ``;
                }
            })();
            fs.writeFileSync(bookmarklet, `javascript:` + encodeURIComponent(transformed));
        }
        // .html
        else if ( fs.existsSync(html) ) {
            const minified = (() => {
                try {
                    return htmlmin(fs.readFileSync(html, `utf-8`), {
                        html5: true,
                        removeStyleLinkTypeAttributes: true,
                        removeScriptTypeAttributes: true,
                        minifyCSS: true,
                        minifyJS: (text, inline) => {
                            if (inline) return text;
                            return babel.transform(text, packagejson.babel).code;
                        },
                        removeComments: true,
                        collapseWhitespace: true,
                        removeTagWhitespace: true,
                    });
                } catch ( error ) {
                    console.error(error);
                    return ``;
                }
            })();
            fs.writeFileSync(bookmarklet, `data:text/html,` + encodeURIComponent(minified));
        }
    }
    console.log(``);
    if ( flag.buildAll ) {
        console.log(indent() + `***************************`);
        console.log(indent() + `******** BUILD ALL ********`);
        console.log(indent() + `***************************`);
    } else {
        console.log(indent() + `********************************`);
        console.log(indent() + `******** BUILD COMPLETE ********`);
        console.log(indent() + `********************************`);
    }
    console.log(``);
    console.log(indent() + `- Build (Auto)......UPDATE FILES`);
    console.log(indent() + `- Build All.........PRESS ENTER`);
    console.log(``);
    flag.buildAll = false;
    flag.clock = true;
};
const clean = (event, file) => {
    flag.clock = false;
    const bookmarklet = file.replace(/\.[a-z\d]+$/, `.bookmarklet`);
    loggedTime = new Date;
    console.log(buildCounter ? `\n\n` : `\n`, `[!]`, `#` + ++buildCounter, event, file);
    console.log(indent() + `clean -> ` + bookmarklet);
    console.log(indent() + loggedTime.toLocaleString());
    shell.rm(bookmarklet);
    console.log(``);
    console.log(indent() + `********************************`);
    console.log(indent() + `******** CLEAN COMPLETE ********`);
    console.log(indent() + `********************************`);
    console.log(``);
    flag.clock = true;
};



/*
    WATCH
*/
let buildCounter = 0;
watch.createMonitor(`.`, {
    filter: file => (() => {
        switch ( true ) {
            case fs.existsSync(file) && fs.statSync(file).isDirectory():
            case config.target.some(glob => micromatch.isMatch(file, glob)):
                return !config.ignore.some(glob => micromatch.isMatch(file, glob));
            default:
                return false;
        }
    })(),
    ignoreDotFiles: config.ignoreDotFiles,
    interval: 0.1,
}, monitor => monitor
    .on(`created`, file => [`.js`, `.html`].includes(path.extname(file)) && build(`created`, file))
    .on(`changed`, file => [`.js`, `.html`].includes(path.extname(file)) && build(`updated`, file))
    .on(`removed`, file => {
        if (flag.buildAll) return;
        const js = file.replace(/\.[a-z\d]+$/, `.js`);
        const html = file.replace(/\.[a-z\d]+$/, `.html`);
        const bookmarklet = file.replace(/\.[a-z\d]+$/, `.bookmarklet`);
        switch ( path.extname(file) ) {
            case `.js`:
            case `.html`:
                if (fs.existsSync(bookmarklet)) clean(`removed`, file);
                break;
            case `.bookmarklet`:
                if (fs.existsSync(js) || fs.existsSync(html)) build(`removed`, file);
                break;
        }
    }
));



/*
    ENTER TO BUILD ALL
*/
const read = readline.createInterface({input: process.stdin});
read.on(`line`, () => {
    flag.buildAll = true;
    build();
});
