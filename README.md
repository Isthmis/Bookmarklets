# Bookmarklets


## ◆ Development

### Dependencies
- Build script
    - Node.js (10.6.0)
    - npm (6.2.0)
    
    `build.command` or `$ npm start`

Developed on macOS El Capitan (10.11.6).


## ◆ License

This project is licensed under the terms of the CC0-1.0 Universal license.

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg "CC0")](https://creativecommons.org/publicdomain/zero/1.0/)
