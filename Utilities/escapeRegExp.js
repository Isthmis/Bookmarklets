void (() => {
    `use strict`;
    const text = prompt(`escapeRegExp`);
    const escapeRegExp = s => String(s).replace(/[\\^$*+?.()|\[\]{}]/g, `\\$&`);
    text && prompt(`Escaped.`, escapeRegExp(text));
})();
