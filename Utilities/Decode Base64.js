void (() => {
    `use strict`;
    const text = prompt(`Decode Base64`);
    const decodeBase64 = string => decodeURIComponent(escape(atob(string)));
    text && prompt(`Decoded.`, decodeBase64(text));
})();
