void (() => {
    `use strict`;
    const published_time = (() => {
        const meta = document.head.querySelector(`meta[property="article:published_time"]`);
        return meta && meta.content;
    })();
    const modified_time = (() => {
        const meta = document.head.querySelector(`meta[property="article:modified_time"]`);
        return meta && meta.content;
    })();

    alert(
        `Publish/Modified Time\n` +
        `\n` +
        `・meta[property="article:published_time"]……${published_time}\n` +
        `・meta[property="article:modified_time"]……${modified_time}\n` +
        `・document.lastModified……${document.lastModified}`
    );
})();
