void (() => {
    `use strict`;
    const text = prompt(`escapeRegExp (+Slash)`);
    const escapeRegExp = s => String(s).replace(/[\\^$*+?.()|\[\]{}\/]/g, `\\$&`);
    text && prompt(`Escaped.`, escapeRegExp(text));
})();
