void (() => {
    `use strict`;

    const URL = location.href;
    const pureURL = location.protocol + `//` + location.host + location.pathname;

    URL !== pureURL && location.assign(pureURL);
})();
